module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'global-require': 'off',
    'no-new': 0, // Intrusive when using Chart.js instances.
    'no-underscore-dangle': 0, // Chart.js uses underscore dangles (_) internally.
    'import/no-unresolved': 0, // False positives regarding imports that use aliases.
    'import/no-named-as-default-member': 'off',
    'import/no-named-as-default': 'off',
    'import/extensions': 'off',
    'import/no-duplicates': 'off',
    'import/no-extraneous-dependencies': 'off',
    'import/first': 'off',
    'func-names': 'off',
    'no-plusplus': 'off',
    eqeqeq: 'off',
  },
  overrides: [
    {
      files: ['*.vue'],
      rules: {
        'max-len': 'off',
      },
    },
    {
      files: ['*.js'],
      rules: {
        'max-len': 'off',
        'no-param-reassign': 'off',
        'no-shadow': 'off',
        camelcase: 'off',
      },
    },
    {
      files: ['*.json'],
      rules: {
        'max-len': 'off',
      },
    },
    {
      files: ['service-worker.js'],
      rules: {
        'max-len': 'off',
        'no-undef': 'off',
        'no-restricted-globals': 'off',
      },
    },
  ],
  parserOptions: {
    parser: 'babel-eslint',
  },
};
