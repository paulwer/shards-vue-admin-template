module.exports = {
  lintOnSave: true,
  pwa: {
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'src/service-worker.js',
      // exclude: [
      //   /\.map$/,
      //   /manifest\.json$/,
      // ],
    },
    themeColor: '#1da025',
    name: 'IKS Ticket System',
  },
  pluginOptions: {
    moment: {
      locales: [
        'en',
        'de',
      ],
    },
  },
};
