import Vue from 'vue';
import Vuex from 'vuex';
import API from '@/plugins/api.js';
import jwt from 'jsonwebtoken';
import VueOffline from 'vue-offline';
// eslint-disable-next-line
import notification_module from './notification_module';


Vue.use(API);
Vue.use(Vuex);
Vue.use(VueOffline, {
  mixin: true, // use the isOnline/isOffline Proterty and Events
  storage: false, // do not use the offline storgae, we ca access it with direct import
});
const store = new Vue(); // use the store var for request api
// Actions, Mutations and and getters of submodules have to expose like: <modulename>_<othername>


export default new Vuex.Store({
  state: {
    loadingStatus: 'notloaded',
    apiStatus: 'notloaded',
    authentificated: false,
    accessToken: localStorage.getItem('accessToken') || null,
    sessionData: null,
  },
  getters: {
    sessionData(state) {
      if (!state.sessionData) throw new Error('SessionData does not exist');
      return state.sessionData;
    },
    loaded(state) {
      return state.loadingStatus.toLowerCase() == 'ready' && state.apiStatus.toLowerCase() == 'ok';
    },
    loadingStatus(state) {
      return state.loadingStatus;
    },
    apiStatus(state) {
      return state.apiStatus;
    },
    connectionError(state) {
      return state.loadingStatus.toLowerCase() == 'connection error';
    },
    loggedIn(state) {
      return state.authentificated;
    },
    Token(state) {
      return state.accessToken;
    },
    TokenData(state) {
      if (!state.accessToken) throw new Error('Token does not exist');
      return jwt.decode(state.accessToken);
    },
  },
  mutations: {
    // Syncronys
    setLoadingStatus(state, status) {
      state.loadingStatus = status;
    },
    setAPIStatus(state, status) {
      state.apiStatus = status;
    },
    destroySession(state) {
      state.authentificated = false;

      // Unset Authorization-Header for API and AccessToken
      delete store.$api.defaults.headers.common.Authorization;
      state.accessToken = null;
      localStorage.removeItem('accessToken');

      // Remove User Data
      state.sessionData = null;
      state.rights = [];

      // Reset Notifications
      this.commit('resetNotifications');
    },
    createSession(state, token) {
      const TokenData = jwt.decode(token);
      if (TokenData.exp <= new Date().getTime() / 1000) {
        throw new Error('Cannot set Token. The Token was expired. Please make sure. You can only set valid Tokens!');
      }
      localStorage.setItem('accessToken', token);
      state.accessToken = token;

      // Setup Token Expiration Handler
      setTimeout(() => {
        if (state.accessToken == token) {
          console.log('Your Token expired. Please relogin...');
          this.dispatch('logout');
        }
      }, (TokenData.exp * 1000) - new Date().getTime());
      console.log(`System will check if this token is valid in ${((TokenData.exp * 1000) - new Date().getTime()) / 1000} seconds`);

      // Set Authorization-Header for API
      store.$api.defaults.headers.common.Authorization = token;
      state.sessionData = TokenData.user;
      state.rights = TokenData.rights;
      state.authentificated = true;
    },
  },
  actions: {
    // asyncronys or syncronys
    async startup(context) {
      context.commit('setLoadingStatus', 'App startet...');
      await store.$api.get('')
        .catch((err) => {
          console.log(err);
          context.commit('setAPIStatus', 'Connection Error');
          return setTimeout(() => {
            context.dispatch('startup');
          }, 3000);
        });
      context.commit('setAPIStatus', 'OK');
      if (context.getters.Token == null) { // NoToken
        context.commit('destroySession');
        context.commit('setLoadingStatus', 'Ready');
        return true;
      }
      console.log('Token found...');
      const { TokenData } = context.getters;
      if (TokenData.exp <= new Date().getTime() / 1000) { // Token is invalid
        context.commit('destroySession');
        console.log('Token expired...');
        context.commit('setLoadingStatus', 'Token abgelaufen. Bitte erneut einloggen...');
        context.commit('setLoadingStatus', 'Ready');
        return true;
      }
      console.log('Try to recover Session');
      context.commit('setLoadingStatus', 'Versuche Session wiederherzustellen...');
      context.commit('createSession', context.getters.Token);
      // Weitere Prüfungen

      // Authorized --> Session established
      context.commit('setLoadingStatus', 'Ready');

      // Init Notifications
      context.dispatch('initNotifications');
      return true;
    },
    logout(context) {
      if (context.getters.loggedIn) {
        context.commit('destroySession');
        return true;
      }
      throw new Error('NO_SESSION - No User was logged in.');
    },
    async login(context, credentials) {
      if (store.isOffline) throw new Error('NO_CONNECTION');
      const response = await store.$api.get(`login?input=${credentials.user}&password=${credentials.password}`);
      context.commit('createSession', response.data.accessToken);

      // Init Notifications
      context.dispatch('initNotifications');
      return response;
    },
  },
  modules: {
    notification: notification_module,
  },
});
