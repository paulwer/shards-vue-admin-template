import Vue from 'vue';
import API from '@/plugins/api.js';
import router from '../router';
import GlobalMethod from '@/plugins/globals';
// Check for displaying default notifications
import notificationPlugin from '../plugins/notification-api';
import Swal from 'sweetalert2';

const randomstring = require('randomstring');

Vue.use(API);
Vue.use(GlobalMethod);

// eslint-disable-next-line
const store = new Vue(); // use the store var for request api
const notification_module = {
  state: {
    notifications: [],
  },
  mutations: {
    /**
     * Stages:
     *  1 - LocalStorage State (identified by user_id and id) -> load Data, if not givven in vars (from last load)
     *  2 - Vuex State    (identified by id)                  -> load Data from existing notifications and override localStorage State
     *  ( if FORCE is enabled Stage 1 and 2 will be ignored)
     *  3 - Load Default Values, when not givven before && Check Validity
     */
    addNotification(state, {
      id,
      issuer,
      user,
      title,
      text,
      short_text,
      onClick,
      active,
      unviewed,
      unclicked,
      timestamp,
      material_icon, // View: https://material.io/resources/icons/
      // MORE
      to, // If this property is declared a route.push() handler is been created
      // Force
      force = false,
      notificationOnWindowUnfocused = false,
    } = {}) {
      // If id is undefined generate one
      if (id === undefined) id = randomstring.generate(16);
      if (typeof user !== 'number') throw new Error('INVALID_PARAMETER(user) - You have to provide the ID of the current user');

      // Check EXISTENCE(LOCAL) & MAP if exists?
      const localnotifications = JSON.parse(localStorage.getItem('notifications')) || [];
      if (localnotifications.find(elem => (elem.id == id && elem.user == user))) {
        if (!(force)) {
          // If noForce, get old date, if not defined in vars
          var elem = localnotifications.find(elem => (elem.id == id && elem.user == user));
          for (var key in elem) {
            switch (key) {
              case 'issuer':
                if (issuer === undefined) issuer = elem[key];
                break;
              case 'title':
                if (title === undefined) title = elem[key];
                break;
              case 'text':
                if (text === undefined) text = elem[key];
                break;
              case 'short_text':
                if (short_text === undefined) short_text = elem[key];
                break;
              case 'timestamp':
                if (timestamp === undefined) timestamp = elem[key];
                break;
              case 'active':
                if (active === undefined) active = elem[key];
                break;
              case 'unviewed':
                if (unviewed === undefined) unviewed = elem[key];
                break;
              case 'unclicked':
                if (unclicked === undefined) unclicked = elem[key];
                break;
              case 'material_icon':
                if (material_icon === undefined) material_icon = elem[key];
                break;
            }
          }
        }
      }

      // Check EXISTENCE & MAP if exists?
      if (state.notifications.find(elem => (elem.id == id))) {
        if (!(force)) {
          // If noForce, get old date, if not defined in vars
          var elem = state.notifications.find(elem => (elem.id == id));
          for (var key in elem) {
            switch (key) {
              case 'issuer':
                if (issuer === undefined) issuer = elem[key];
                break;
              case 'title':
                if (title === undefined) title = elem[key];
                break;
              case 'text':
                if (text === undefined) text = elem[key];
                break;
              case 'short_text':
                if (short_text === undefined) short_text = elem[key];
                break;
              case 'timestamp':
                if (timestamp === undefined) timestamp = elem[key];
                break;
              case 'active':
                if (active === undefined) active = elem[key];
                break;
              case 'unviewed':
                if (unviewed === undefined) unviewed = elem[key];
                break;
              case 'unclicked':
                if (unclicked === undefined) unclicked = elem[key];
                break;
              case 'onClick':
                if (onClick === undefined) onClick = elem[key];
                break;
              case 'material_icon':
                if (material_icon === undefined) material_icon = elem[key];
                break;
            }
          }
        }
        // Delete Notification from Vuex State
        state.notifications = state.notifications.filter(elem => (elem.id !== id));
      }
      // Map defauls, when undefined
      if (issuer === undefined) issuer = null;
      if (short_text === undefined) short_text = null;
      if (timestamp === undefined) timestamp = (new Date()).getTime();
      if (active === undefined) active = true;
      if (unviewed === undefined) unviewed = true;
      if (unclicked === undefined) unclicked = true;
      if (onClick === undefined) onClick = function () {};
      if (material_icon === undefined) material_icon = 'info';

      // Check Datatypes
      if (issuer !== null && typeof issuer !== 'string') throw new Error('INVALID_PARAMETER(issuer) - Issuer have to be Type of String.');
      if (typeof title !== 'string') throw new Error('INVALID_PARAMETER(title) - Title have to be Type of String.');
      if (typeof text !== 'string') throw new Error('INVALID_PARAMETER(text) - Text have to be Type of String.');
      if (short_text !== null && typeof short_text !== 'string') throw new Error('INVALID_PARAMETER(short_text) - short_text have to be Type of String.');
      if (typeof timestamp !== 'number') throw new Error('INVALID_PARAMETER(timestamp) - Timestamp have to be a Number.');
      if (typeof active !== 'boolean') throw new Error('INVALID_PARAMETER(active) - Active have to be Type of Boolean.');
      if (typeof unviewed !== 'boolean') throw new Error('INVALID_PARAMETER(unviewed) - Unviewed have to be Type of Boolean.');
      if (typeof unclicked !== 'boolean') throw new Error('INVALID_PARAMETER(unclicked) - Unclicked have to be Type of Boolean.');
      if (onClick && typeof onClick !== 'function') throw new Error('INVALID_PARAMETER(onClicked) - onClicked have to be Type of Function.');
      if (typeof material_icon !== 'string') throw new Error('INVALID_PARAMETER(material_icon) - material_icon have to be Type of String.');

      // MORE
      if (to) {
        if (typeof to !== 'string') throw new Error('INVALID_PARAMETER(to) - To have to be Type of String.');
        if (onClick == function () {}) throw new Error("INVALID_PARAMETER(to) - A onClick handler is already defined and cannot be generated by 'to'-Parameter.");
        onClick = function () {
          try {
            // Try Routing with path
            router.push({ path: to });
          } catch (err) {
            try {
              // Try routing with route-name
              router.push(to);
            } catch (err) {
              throw new Error(`NOTIFICATION_ERROR - An Error accures while try to execute route.push to '${to}' as path or routing name. Issuer: ${issuer} ID: ${id}`);
            }
          }
        };
      }
      // ADD it to array
      state.notifications.push({
        id,
        user,
        title,
        text,
        short_text,
        material_icon,
        onClick: () => {
          onClick();
          // If the Click-Handler was successfull, mark the notification as clicked
          this.commit('clickNotification', id);
        },
        timestamp,
        active,
        unviewed,
        unclicked,
      });
      localStorage.setItem('notifications', JSON.stringify(state.notifications));

      // Optional Notification if granted and if window is not focused
      if (notificationOnWindowUnfocused) {
        if (notificationPlugin.activated()) {
          notificationPlugin.sendNotification({
            title,
            text,
            tag: id,
          }, false);
        }
      }
    },
    viewAllNotifications(state) {
      state.notifications = state.notifications.map((elem) => {
        elem.unviewed = false;
        return elem;
      });
      localStorage.setItem('notifications', JSON.stringify(state.notifications));
    },
    viewNotification(state, id) {
      if (!(state.notifications.find(elem => elem.id === id))) throw new Error(`NOT_FOUND(${id})`);
      state.notifications = state.notifications.map((elem) => {
        if (elem.id == id) elem.unviewed = false;
        return elem;
      });
      localStorage.setItem('notifications', JSON.stringify(state.notifications));
    },
    clickNotification(state, id) {
      if (!(state.notifications.find(elem => elem.id === id))) throw new Error(`NOT_FOUND(${id})`);
      state.notifications = state.notifications.map((elem) => {
        if (elem.id == id) elem.unclicked = false;
        return elem;
      });
      localStorage.setItem('notifications', JSON.stringify(state.notifications));
    },
    deactivateNotification(state, id) {
      if (!(state.notifications.find(elem => elem.id === id))) throw new Error(`NOT_FOUND(${id})`);
      state.notifications = state.notifications.map((elem) => {
        if (elem.id == id) elem.active = false;
        return elem;
      });
      localStorage.setItem('notifications', JSON.stringify(state.notifications));
    },
    deleteNotification(state, id) {
      if (!(state.notifications.find(elem => elem.id === id))) throw new Error(`NOT_FOUND(${id})`);
      state.notifications = state.notifications.filter(elem => !elem.id == id);
      localStorage.setItem('notifications', JSON.stringify(state.notifications));
    },
    resetNotifications(state) {
      state.notifications = [];
    },
  },
  actions: {
    initNotifications(context) {
      context.dispatch('setupWindowNotifications');
      context.dispatch('updateNotifications');
    },
    updateNotifications(context) {
      // Neues Ticket zugeordnet
      // Tickets in Arbeit
    },
    setupWindowNotifications(context) {
      function askNotificationPermission() {
        if (notificationPlugin.available()) {
          notificationPlugin.activate(true)
            .then((permission) => {
              console.log('Permission', permission);
              if (permission == 'granted') context.commit('deleteNotification', 'ask_push_notification_request');
            });
          if (Notification.permission == 'denied') {
            Swal.fire({
              text: 'Falls das Abfragefenster nicht automatisch erschienen ist, müssen Sie Benachichtigungen in den Einstellungen manuell aktivieren',
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 5000,
            });
          }
        }
      }
      if (notificationPlugin.available() && (!Notification.permission || Notification.permission == 'default' || Notification.permission == 'denied')) {
        context.commit('addNotification', {
          id: 'ask_push_notification_request',
          user: context.getters.sessionData.id,
          issuer: 'system',
          title: '(Push-)Benachichtigungen',
          text: 'Erlaube dieser App dir Benachichtigungen für wichtige Ereignisse zu senden. Wir senden dir Benachichtigungen zu Tickets und anstehenden Aufgaben.',
          short_text: 'Erlaube dieser App dir Benachichtigungen für wichtige Ereignisse zu senden.',
          onClick: askNotificationPermission,
          material_icon: 'speaker_notes',
          timestamp: (new Date()).getTime(),
        });
      } else if (notificationPlugin.available()) {
        store.$apimethod.push_registration();
      }
    },
  },
  getters: {
    notifications(state) {
      return state.notifications;
    },
    notificationsUnviewed(state, getters) {
      return getters.notificationsActive.filter(elem => elem.unviewed);
    },
    notificationsUnclicked(state, getters) {
      return getters.notificationsActive.filter(elem => elem.unclicked);
    },
    notificationsActive(state) {
      return state.notifications.filter(elem => elem.active);
    },
    notificationsInactive(state) {
      return state.notifications.filter(elem => !elem.active);
    },
    notificationsFrom: state => issuer => state.notifications.filter(elem => elem.issuer === issuer),
    issuers(state) {
      const issuers = [];
      state.notifications.forEach((elem) => {
        if (!issuers.find(issuer => elem.issuer === issuer)) issuers.push(elem.issuer);
      });
      return issuers;
    },
    count(state, getters) {
      return getters.notifications.length;
    },
    countUnviewed(state, getters) {
      return getters.notificationsUnviewed.length;
    },
    countUnclicked(state, getters) {
      return getters.notificationsUnclicked.length;
    },
    countActive(state, getters) {
      return getters.notificationsActive.length;
    },
    countInactive(state, getters) {
      return getters.notificationsInactive.length;
    },
  },
};
export default notification_module;
