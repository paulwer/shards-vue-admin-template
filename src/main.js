/* eslint-disable */
import Vue from 'vue';
import ShardsVue from 'shards-vue';

// Plugins
import API from '@/plugins/api.js';
import APIMethod from '@/plugins/apimethod.js';
import GlobalMethod from '@/plugins/globals.js';
import './plugins/bootstrap-vue'
import VueSweetalert2 from 'vue-sweetalert2';
const moment = require('moment')

Vue.use(VueSweetalert2);
Vue.use(API);
Vue.use(APIMethod);
Vue.use(GlobalMethod);
require('moment/locale/de')
Vue.use(require('vue-moment'), {
  moment
})

// Styles
import 'bootstrap/dist/css/bootstrap.css';
import '@/scss/shards-dashboards.scss';
import '@/assets/scss/date-range.scss';

// Core
import App from './App.vue';
import router from './router';
import store from './store';

// Layouts
import Default from '@/layouts/Default.vue';

// PWA
import './registerServiceWorker';
import VueOffline from 'vue-offline';
Vue.use(VueOffline, {
  mixin: true,    // use the isOnline/isOffline Proterty and Events
  storage: false  // do not use the offline storgae, we ca access it with direct import
});

// Sentry Error Logger
import * as Sentry from '@sentry/browser';
import { Vue as VueIntegration } from '@sentry/integrations';
if (process.env.NODE_ENV == "production")
  Sentry.init({
    dsn: process.env.SENTRY_URL,
    integrations: [new VueIntegration({ Vue, attachProps: true })],
    release: `${process.env.npm_package_name}@${process.env.npm_package_version}`,
    environment: process.env.NODE_ENV
  });

ShardsVue.install(Vue);

Vue.component('default-layout', Default);

Vue.config.productionTip = false;
Vue.prototype.$eventHub = new Vue();
console.log(process.env);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
