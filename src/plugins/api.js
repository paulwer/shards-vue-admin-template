import axios from 'axios';

if (!process.env.VUE_APP_API_BASE_URL) throw new Error('You have to provide env.: VUE_APP_API_BASE_URL');

// All additional defaults, like URL and Token(Authorization-Header) will be set in Vuex index.js
const API = {
  install(Vue) {
    Vue.prototype.$api = axios.create({
      baseURL: process.env.VUE_APP_API_BASE_URL,
      responseType: 'json',
    });
  },
};

export default API;
