// import Vue from 'vue';
import Swal from 'sweetalert2';

// const app = new Vue();
const GlobalMethod = {
  install(Vue) {
    Vue.prototype.$globalmethod = {
      // eslint-disable-next-line
      haveRight(input, toast) {
        // return true or false (sync)
        // TODO
      },
      // eslint-disable-next-line
      haveRole(input, toast) {
        // return true or false (sync)
        // execute a call to api(apimethod) if completly not found
        // TODO
      },
      toast_missing_right(input) {
        if (input) {
          Swal.fire({
            toast: true,
            position: 'top-end',
            icon: 'warning',
            title: `Berechtigung: ${input} fehlt.`,
            showConfirmButton: false,
            timer: 3000,
          });
        }
      },
      toast_missing_role(input) {
        if (input) {
          Swal.fire({
            toast: true,
            position: 'top-end',
            icon: 'warning',
            title: `Rolle: ${input} fehlt.`,
            showConfirmButton: false,
            timer: 3000,
          });
        }
      },
      toast_internal_server_error() {
        Swal.fire({
          toast: true,
          position: 'top-end',
          icon: 'error',
          title: 'Anfrage nicht erfolgreich. Fehler auf Serverseite. Bitte erneut versuchen.',
          showConfirmButton: false,
          timer: 3000,
        });
      },
      toast_unknown_error() {
        Swal.fire({
          toast: true,
          position: 'top-end',
          icon: 'question',
          title: 'Es ist ein unbekannter Fehler aufgetreten.',
          showConfirmButton: false,
          timer: 3000,
        });
      },
      toast_success(title, timer = 3000) {
        Swal.fire({
          toast: true,
          position: 'top-end',
          icon: 'success',
          title,
          showConfirmButton: false,
          timer,
        });
      },
      toast_info(title, timer = 3000) {
        Swal.fire({
          toast: true,
          position: 'top-end',
          icon: 'info',
          title,
          showConfirmButton: false,
          timer,
        });
      },
    };
  },
};

export default GlobalMethod;
